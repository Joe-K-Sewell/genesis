@echo off
setlocal

if not [%CD%\]==[%~dp0] (
    echo You need to run with this script's directory as the working directory.
    exit /b 1
)

echo Checking for asm68k...
where /Q asm68k
if %errorlevel% neq 0 (
    echo ERROR: Could not find asm68k on the PATH. 1>&2
    echo        This is needed to assemble Motorola 68000 assembly code. 1>&2
    exit /b 1
)

echo Checking for yaza...
where /Q yaza
if %errorlevel% neq 0 (
    echo ERROR: Could not find yaza on the PATH. 1>&2
    echo        This is needed to assemble Zilog Z80 assembly code. 1>&2
    exit /b 1
)

echo Checking for MSBuild...
where /Q msbuild
if %errorlevel% neq 0 (
    echo ERROR: Could not find MSBuild on the PATH. 1>&2
    echo        This is needed to build prerequisites in the Tools directory, during this script's execution only, it doesn't have to be on the PATH when building the projects. 1>&2
    exit /b 1
)

echo Building prerequisites
msbuild Tools/PaletteLibrary/PaletteLibrary.csproj /t:Restore
if %errorlevel% neq 0 (
    echo ERROR: Failed to restore packages for PaletteLibrary.csproj. 1>&2
    exit /b 1
)
msbuild Tools/TextRectangleToBinary/TextRectangleToBinary.csproj /t:Restore
if %errorlevel% neq 0 (
    echo ERROR: Failed to restore packages for TextRectangleToBinary.csproj. 1>&2
    exit /b 1
)
msbuild Tools/Tools.sln /t:Build
if %errorlevel% neq 0 (
    echo ERROR: Failed to build Tools.sln. 1>&2
    exit /b 1
)

echo Everything Looks Good to Go
echo Build project by running build.bat in this directory
exit /b 0