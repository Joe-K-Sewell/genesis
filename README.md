This is a very simple homebrew game for the Sega Genesis / Mega Drive.
I made it for presentation at [CodeMash 2020](https://www.codemash.org/),
both as part of the session
[**Assembly Language: The (Sega) Genesis of Programming**][talk]
and at [my employer's booth][booth].
(Note that this project is not sponsored by my employer;
it is a personal project otherwise unassociated with my day job.)

You might want to review my session's [slides][slides] or 
[watch the presentation on Pluralsight][video] (for free, no account needed).

## About the Game

**The CodeMash Game** (for which I have no better name) is a platformer with
programmer art. You control a block of digits and can move left or right and
jump. Your goal is to use the platforms to reach all of the target objects
(indicated by the letter `T`) as fast as you can.

This game lacks several features, such as:

* Background art
* Animations
* Multiple levels
* Levels larger than the plane size for the Genesis
* Vertical scrolling
* Title screen / menus
* Ability to restart a level without resetting the console
* Sound

## Prerequisites & First-Time Setup

Here's what you need to do in order to build the code from this repository.

1. Be running Windows.
   (Some tools, including the 68000 assembler, are specific to Windows.)

2. Have Visual Studio or an MSBuild capable of building .NET Framework 4.7.2
   console apps and .NET Standard 2.0 libraries.

3. Download the following and ensure they are on your PATH:

    * [asm68k][asm68k], a Motorola 68000 assembler
      (you can open the download package with [7-Zip][7z])

    * [yaza][yaza], a Zilog Z80 assembler

4. Clone this repository if you haven't already.

5. Open a command prompt that has access to `msbuild`, such as 
   *Developer Command Prompt for VS 2019*.

6. Change directory to this repository's root.

7. Run `setup.bat` and verify that "Everything Looks Good to Go" is printed.

## Building

Once you've set up the repository, you can build by doing the following:

1. Open a command prompt.
   (It does not need to have access to `msbuild`.)

2. Change directory to this repository's root.

3. Run `build.bat` and verify that "Build OK" is printed.

    * You can clean by supplying `--clean` to the batch script.

4. The `CodeMash.BIN` file can be used on a Sega Genesis / Mega Drive emulator
   or flashcart.

5. The `CodeMash.L68` file is an annotated version of what got assembled.
   The first column lists the relevant address for the line, the second column
   (if it's not blank) lists the value that got assembled, and the remaining
   text on the line is the corresponding assembly code line.

   You can use this file to help with debugging in an emulator, e.g.,
   [Exodus][exodus]. Find the line of assembly code you want to breakpoint,
   then set a breakpoint in the emulator on the corresponding address.

## Developing

You primarily develop this repository via a plain text editor
(I use Visual Studio Code, with the [m68k extension][extension])
to edit the various assembly (`.S` and `.ASM`) files and the `.TXT` format
level data file.

To modify graphics, use [Tile Layer Pro][tlppro].
(Despite the name, it's freeware.)
Use it like this:

1. *File* -> *Open...*, select a `.BIN` file from the `Patterns/` directory.

2. *Palette* -> *Load...*, select a `.TPL` file from the `Palettes/` directory.

3. Modify the graphics as you choose in the Tile Layer Pro editor.
   Note that the pattern files have fixed sizes; if you want to add more
   patterns, initialize an empty file with the appropriate length (each pattern
   takes 32 bytes). Data beyond the limit of the file is displayed as garbage.
   No idea if modifying that garbage is dangerous, so **don't modify garbage
   data in the editor**.

4. *File* -> *Save* to save the pattern data.

5. *Palette* -> *Save* to save the palette data.

### Repository Layout

The following is a list of the files in the repository.
Files marked **(generated)** are created by setup or a build, and are ignored by
the git repository.

* Root directory
  
    * `README.md` - This file.
      The `.md` extension means "MarkDown". MD is also the abbreviation for
      the Genesis' international name, Mega Drive, so if you see it in another
      context in this repository it probably means "Mega Drive", not "MarkDown".

    * `setup.bat` - Windows batch script to set up the project for building.

    * `build.bat` - Windows batch script to build the project.

    * `*.S` - Motorola 68000 assembly language source files.
    This is the main source code for the game; see the header for each to
    understand its purpose and scope.

    * `CodeMash.BIN` **(generated)** - Motorola 68000 machine code program.
      For the Genesis, this is called a "ROM image file", because it's a file
      representing what gets built into the game cartridge's ROM chips.
      I've never done that, though; I recommend either loading the file onto
      an emulator for the PC or onto a flashcart for a real Genesis system.

    * `CodeMash.L68` **(generated)** - listing file for debugging.
      See above for a description of this file.

* `LevelData\`
  
    * `*.TXT` - ASCII art representing a level.
      The `-` symbol indicates a platform, and `T` indicates a target object.

    * `*.BIN` **(generated)** - binary level data generated from the 
      corresponding ASCII art `.TXT` file.

* `Palettes\`

    * `*.TPL` - palette data for Tile Layer Pro.
    
    * `*.TPL.BIN` **(generated)** - Genesis-format palette line data generated
      from the corresponding Tile Layer Pro file.

* `Patterns\`

    * `*.BIN` - Genesis-format pattern data, also compatible with 
      Tile Layer Pro as-is.

* `System\`

    * `*.S` - "system level" Motorola 68000 source files.
      This repository started out as a multiple-project repository, with
      shared code in a "system" directory. I've kept that distinction to
      make it clear that these files, while important, don't have much of
      the "real" game code - it's mostly boilerplate. Still, feel free
      to look around.

* `Tools\`

    * `Tools.sln` - Visual Studio solution file for the tool projects in this
      directory. Each project contains C# source code as well as **generated**
      .NET assemblies, etc. If you remove these assemblies, you will need to
      run `setup.bat` again.

* `Z80\`

    * `*.ASM` - Zilog Z80 assembly language source files.
      This is for the Genesis' secondary processor, which this game mostly
      leaves unused. The only code is to get the processor into a consistent
      state and then loop forever doing nothing.

    * `*.BIN` **(generated)** - Zilog Z80 machine code program.
      Since the Genesis' Z80 chip is usually tasked with managing audio,
      this is also called the "sound driver". It gets embedded into the
      main Motorola 68000 machine code program, which is responsible for
      loading the Z80 program onto the chip during system startup.

### Naming Conventions

Since I learned to program in strongly-typed, statically-typed languages,
I use a form of [Hungarian notation][hungarian] in the assembly that I write.
Here are a list of prefixes and what they mean.

| Prefix    | Meaning                                               |
|-----------|-------------------------------------------------------|
| (none)    | Address to code (something you can `JMP` to)          |
| `R`       | Address to subroutine (something you can `JSR` to)    |
| `M`       | Macro                                                 |
| `P`       | Constant parameter for a macro                        |
| `Bc`      | Constant value that can fit in a byte                 |
| `Wc`      | Constant value that can fit in a word                 |
| `Lc`      | Constant value that can fit in a longs                |
| `Ba`      | Address that should be accessed as a byte             |
| `Wa`      | Address that should be accessed as a word             |
| `La`      | Address that should be accessed as a longs            |
| `Bt`      | Address to a table (array) to be accessed as bytes    |
| `Wt`      | Address to a table (array) to be accessed as words    |
| `Lt`      | Address to a table (array) to be accessed as longs    |
| `Vt`      | Address to a table (array) whose access varies        |
| `Pattern` | Graphical pattern (tiles), 32 bytes                   |
| `Palette` | Graphical palette (colors), 32 bytes                  |

## Resources / References

### Tools

* [EASy68k][easy68k].
  This is a Motorola 68000 testing environment, including a simulator with
  easy-to-set breakpoints, stack view, etc. The documentation is also useful
  for understanding various opcodes and concepts of the assembly language,
  but do note that the *directives* (e.g., `SIMHALT`) are specific to this
  IDE / assembler, and aren't always the same as with the assembler used by
  this project.

### Documentation

* [The 68000's Instruction Set][instructions].
  I'm not sure of the origin - the link goes to an Italian university host of
  the file - but it's a very useful reference to all the opcodes without all
  the implementation details normally found in these manuals. It just tells you
  what each opcode does and why you might use it.
* [Psy-Q Systems Manual][saturn].
  A technical manual for the Sega Saturn PC development kit.
  Contains documentation on ASM68k.
  Mostly useful as it documents the directives the assembler supports.
* [EASy68k][easy68k]'s documentation, as mentioned above with caveats.
* [Motorola M68000 Family Programmer's Reference Manual][68kman].
  A more comprehensive description of the instruction set architecture.
  Note that some material isn't relevant to the original Motorola 68000
  processor, like floating point.
* Sega Retro
    * [Official documentation][official].
      Includes *Genesis Software Manual*.
    * [Unofficial documentation][unofficial].
      Makes the specifics of the Genesis Software Manual more apparent.
    * [Technical specifications][techspecs].

### Sample Code

* GameHut: 
  [Will You Code the Next Sonic for SEGA Genesis? I Teach You How #1][gamehut].
  Includes a template for the "system" stuff that I'm shamelessly
  cargo-culting from.
* Marc Haisenko: [Programming the Sega MegaDrive][haisenko].
  Explains how to initialize the system.

### My Cheat Sheet for Conditionals

`CMP A,B` is the same as `SUB A,B` but it doesn't affect B, it only affects the
status register's flags. You can then use operands in the form `Bxx` to branch
based on the result.

**Important**: The thing to get used to is that the ordering of the operands is
"backward" from how you would normally write it. So `CMP A,B` followed by `BLT`
means B < A, not A < B. This is because `SUB A,B` means B = B - A.
(It was at this point that I realized that putting the destination first might
 have been a better choice than the way the 68000 does it.)

| comparison | kind     | opcodes   | formal def        | name |
| ---------- | -------- | --------- | ----------------- | --- |
| B < A      | signed   | BLT       | N != V            | Less Than |
| B < A      | unsigned | BLO (BCS) | C == 1            | LOwer (Carry Set) |
| B <= A     | signed   | BLE       | Z == 1 or N != V  | Less than or Equal |
| B <= A     | unsigned | BLS       | C == 1 or Z == 1  | Lower or Same |
| B == A     | both     | BEQ       | Z == 1            | EQual |
| B != A     | both     | BNE       | Z == 0            | Not Equal |
| B > A      | signed   | BGT       | N == V and Z == 0 | Greater Than |
| B > A      | unsigned | BHI       | C == 0 and Z == 0 | HIgher than |
| B >= A     | signed   | BGE       | N == V            | Greater than or Equal |
| B >= A     | unsigned | BHS (BCC) | C == 0            | Higher or Same (Carry Clear) |

You can also use `TST B`, which is the same as `CMP #0,B`.
There are some additional comparisons you can use when comparing to zero:

| comparison | kind     | opcodes        | formal def        | name | other opcodes |
| ---------- | -------- | -------------- | ----------------- | --- | --- |
| B < 0      | signed   | BMI            | N == 1            | MInus | BLT |
| B == 0     | both     | BZ             | Z == 1            | Zero | BEQ |
| B != 0     | both     | BNZ            | Z == 0            | Not Zero | BNE |
| B > 0      | signed   | BPL            | N == 0            | PLus | BGE |

You can of course use any of these after other instructions which affect the
flags, using their formal definitions. So to check if you just added two numbers
and it happened to sum to zero, you could use BZ. There are some additional
variants that can also be used this way:

| opcodes        | formal def        | name |
| -------------- | ----------------- | --- |
| BVC            | V == 0            | oVerflow Clear |
| BVS            | V == 1            | oVerflow Set |
| BRA            | always            | (BRanch) Always |

Some information from [here](http://68k.hax.com/CMP).

[talk]:     https://www.codemash.org/session-details/?id=145606
[slides]:   https://docs.google.com/presentation/d/1s5fN3x3Z8sZjxKsu0RDSKzp1bMBEp-1a9MEtawF29QA/edit?usp=sharing
[video]:    https://www.pluralsight.com/courses/codemash-session-97
[booth]:    https://www.preemptive.com/

[asm68k]:   http://info.sonicretro.org/File:ASM68k.7z
[7z]:       https://www.7-zip.org/
[yaza]:     https://github.com/toptensoftware/yazd/blob/master/yaza.md
[exodus]:   http://www.exodusemulator.com/
[tlppro]:   https://segaretro.org/Tile_Layer_Pro

[easy68k]:  http://www.easy68k.com/
[instructions]: http://wpage.unina.it/rcanonic/didattica/ce1/docs/68000.pdf

[68kman]:       https://www.nxp.com/files-static/archives/doc/ref_manual/M68000PRM.pdf
[official]:     https://segaretro.org/Mega_Drive_official_documentation
[unofficial]:   https://segaretro.org/Category:Unofficial_documentation
[techspecs]:    https://segaretro.org/Sega_Mega_Drive/Technical_specifications
[saturn]:       https://antime.kapsi.fi/sega/files/SATMAN.pdf
[haisenko]:     https://darkdust.net/writings/megadrive
[gamehut]:      https://www.youtube.com/watch?v=PSYhSmXBgIw

[hungarian]:    https://en.wikipedia.org/wiki/Hungarian_notation
[extension]:    https://marketplace.visualstudio.com/items?itemName=steventattersall.m68k