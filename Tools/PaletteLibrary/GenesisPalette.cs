﻿using System;

namespace PaletteLibrary
{
    /// <summary>
    /// Represents the contents of the Genesis' color RAM: 4 palette lines of 16 colors each.
    /// </summary>
    public class GenesisColorRAM
    {
        private GenesisPaletteLine[] entries = new GenesisPaletteLine[4];

        public GenesisPaletteLine this[int i]
        {
            get
            {
                ThrowIfIndexOutOfBounds(i);
                return entries[i];
            }
            set
            {
                ThrowIfIndexOutOfBounds(i);
                entries[i] = value;
            }
        }

        private void ThrowIfIndexOutOfBounds(int i)
        {
            if (i < 0 || i >= 4)
            {
                throw new IndexOutOfRangeException($"The Genesis color RAM has 4 color lines, but index {i} was specified");
            }
        }
    }

    /// <summary>
    /// Represents a "palette line" in the Genesis' color RAM.
    /// Consists of 16 colors that can be selected per-pixel in tile data.
    /// </summary>
    public class GenesisPaletteLine
    {
        private GenesisPaletteEntry[] entries = new GenesisPaletteEntry[16];

        public GenesisPaletteEntry this[int i]
        {
            get 
            {
                ThrowIfIndexOutOfBounds(i);
                return entries[i]; 
            }
            set 
            {
                ThrowIfIndexOutOfBounds(i);
                entries[i] = value; 
            }
        }

        private void ThrowIfIndexOutOfBounds(int i)
        {
            if (i < 0 || i >= 16)
            {
                throw new IndexOutOfRangeException($"A Genesis palette line has 16 colors, but index {i} was specified");
            }
        }
    }

    /// <summary>
    /// Represents a Genesis palette entry, consisting of 3-bit blue, green, and red components.
    /// </summary>
    public class GenesisPaletteEntry
    {
        public GenesisPaletteComponent Blue { get; set; }
        public GenesisPaletteComponent Green { get; set; }
        public GenesisPaletteComponent Red { get; set; }


        public override string ToString()
        {
            return $"{nameof(GenesisPaletteEntry)} $0{Blue.Nybble:X}{Green.Nybble:X}{Red.Nybble:X}";
        }

        public byte[] ToBytes()
        {
            // Formatted: 0BGR
            var toRet = new byte[2]
            {
                Blue.Nybble,
                (byte) ((Green.Nybble << 4) | Red.Nybble)
            };
            return toRet;
        }
    }

    /// <summary>
    /// Represents the 3-bit value of one component of a Genesis palette entry.
    /// </summary>
    public class GenesisPaletteComponent : IComparable<GenesisPaletteComponent>
    {
        /// <summary>
        /// A nybble (half-byte) containing the 3-bit color value in its most significant bits
        /// and a 0 in the least significant bit. (This is how the Genesis color components are
        /// stored.)
        /// </summary>
        public byte Nybble { get; }

        private GenesisPaletteComponent(byte nybble)
        {
            Nybble = nybble;
        }

        public static GenesisPaletteComponent Val0 = new GenesisPaletteComponent(0x0);
        public static GenesisPaletteComponent Val2 = new GenesisPaletteComponent(0x2);
        public static GenesisPaletteComponent Val4 = new GenesisPaletteComponent(0x4);
        public static GenesisPaletteComponent Val6 = new GenesisPaletteComponent(0x6);
        public static GenesisPaletteComponent Val8 = new GenesisPaletteComponent(0x8);
        public static GenesisPaletteComponent ValA = new GenesisPaletteComponent(0xA);
        public static GenesisPaletteComponent ValC = new GenesisPaletteComponent(0xC);
        public static GenesisPaletteComponent ValE = new GenesisPaletteComponent(0xE);

        public int CompareTo(GenesisPaletteComponent other)
        {
            return this.Nybble.CompareTo(other.Nybble);
        }

        public override string ToString()
        {
            return $"{nameof(GenesisPaletteComponent)} ${Nybble:X}";
        }
    }
}
