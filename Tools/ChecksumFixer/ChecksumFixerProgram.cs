﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChecksumFixer
{
    /// <summary>
    /// A CLI tool that updates a ROM's checksum based on its contents.
    /// </summary>
    class ChecksumFixerProgram
    {
        static int Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Description: \n\tCalculcates the checksum for a Genesis ROM file and updates the appropriate header value in the ROM.");
                Console.WriteLine("Usage: \n\tChecksumFixer MyRom.BIN");
                return 2;
            }

            try
            {
                var filename = args[0];
                var backupFilename = Path.ChangeExtension(filename, ".BAK.BIN");

                // Calculate checksum
                Console.WriteLine($"Reading {filename}...");
                ushort checksum = 0;
                using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    // Skip IVT and cartridge header, not included in checksum
                    fs.Seek(0x200, SeekOrigin.Begin);

                    // Read each word and sum them https://segaretro.org/Checksum
                    byte[] bytes = new byte[2];
                    int numberRead;
                    while ((numberRead = fs.Read(bytes, 0, bytes.Length)) > 0)
                    {
                        if (numberRead != 2)
                        {
                            throw new ApplicationException($"Somehow read {numberRead} bytes. Does the ROM have an odd number of bytes?");
                        }

                        // Convert bytes to word (big endian)
                        var word = (bytes[0] << 8) | bytes[1];
                        checksum += (ushort) word;
                    }
                }
                Console.WriteLine($"Checksum is 0x{checksum:X4}");

                // Backup ROM
                if (File.Exists(backupFilename))
                {
                    File.Delete(backupFilename);
                }
                File.Move(filename, backupFilename);

                // Update ROM
                int valueTransferred;
                const int checksumLocation = 0x18E;
                using (var inFile = new FileStream(backupFilename, FileMode.Open, FileAccess.Read))
                {
                    using (var outFile = new FileStream(filename, FileMode.Create, FileAccess.Write))
                    {
                        while ((valueTransferred = inFile.ReadByte()) >= 0)
                        {
                            // did we just read the first byte of the checksum location?
                            if (inFile.Position == checksumLocation + 1)
                            {
                                var msb = (byte) ((checksum & 0xFF00) >> 8);
                                outFile.WriteByte(msb);

                                var lsb = (byte)  (checksum & 0x00FF);
                                outFile.WriteByte(lsb);
                                inFile.Seek(1, SeekOrigin.Current); // advance inFile beyond checksum
                            }
                            else
                            {
                                outFile.WriteByte((byte)valueTransferred);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error: {ex}");
                return 1;
            }

            return 0;
        }
    }
}
